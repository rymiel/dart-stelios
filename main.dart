// ignore: implementation_imports
import "package:dart_console/src/key.dart";
import "package:stelios/const.dart";
import "package:stelios/entity.dart";
import "package:stelios/event.dart";
import "package:stelios/item.dart";
import "package:stelios/party.dart";
import "package:stelios/path.dart";
import "package:stelios/platform_specific/base.dart"
    if (dart.library.io) "package:stelios/platform_specific/native.dart"
    if (dart.library.html) "package:stelios/platform_specific/web.dart";
import "package:stelios/screen.dart";

Future<void> run(Screen screen) async {
  screen.write("%fYStelios %FY$versionLong %fY%SUdevelopment version%XE\n");
  final emilia = PlayerEntity("Emilia");
  final sol = PlayerEntity("Sol");
  final party =
      Party(screen, {emilia: FormationPos(0, 0), sol: FormationPos.hero()})
        ..location = Location("Fela", "fela");
  final testPath = Path("test path", [
    RoomUnit(
        RoomSchematic(Event(encounterBattle, [
          [null, "puma", null, null, null, null, null, null, null, null]
        ])),
        1,
        1)
  ]);
  final testItem = Item(name: "Cat ear headband", rarity: Rarity.rare);
  final testItem2 = Item(name: "Glass shard", count: 4, stackSize: 16);
  final testItem3 = Item(name: "Glass shard", count: 15, stackSize: 16);

  await screen.enterToContinue();

  screen
    ..write("${screen.windowWidth} x ${screen.windowHeight}")
    ..write(emilia.inventory.add(testItem))
    ..write(emilia.inventory.add(testItem2))
    ..write(emilia.inventory.add(testItem3), newLines: 1)
    ..write("%fCYou are playing as $emilia and $sol%XE", newLines: 1)
    ..write("$party's inventory:\n${party.inventory}",
        newLines: 1, nonbreaking: true);

  testPath.generateRoomFor(party);
  screen.lock();
}

void main() {
  prepareSchemata().then((_) {
    final screen = Screen("Stelios $versionShort");
    try {
      run(screen).then((_) => screen.quit(0));
      // ignore: avoid_catches_without_on_clauses
    } catch (e, s) {
      screen.renderDialogBox(
          title: "Fatal ${e.runtimeType.toString()}",
          content:
              "$e\n\nThe game didn't except this error and thus cannot continue; it was caught at the top level.\n"
              "Report the full log please.",
          optionNames: [
            "Read full log and exit",
            "Exit game"
          ],
          optionKeys: [
            Key.printable("r"),
            Key.printable("x")
          ],
          optionFunctions: [
                (t) => t.displayAndExit("$s\n\n$e"),
                (t) => t.quit(1)
          ]);
    }
  });
}
