import "dart:math";

import "package:dart_console/src/key.dart";

typedef Len = int Function(String);

extension CenterText on String {
  String loop(int from, [int to, Len lengthFunc]) {
    if (this == "") {
      throw ArgumentError("Input string cannot be null or empty");
    }
    final len = lengthFunc == null ? length : lengthFunc(this);
    final leftFrag = from >= 0 ? from ~/ len : ((from - len) ~/ len);
    to ??= (leftFrag + 1) * len;
    final rightFrag = to - 1 >= 0 ? to ~/ len : ((to - len) ~/ len);
    final fragOffset = rightFrag - leftFrag - 1;
    if (fragOffset == -1) {
      return substring(from - leftFrag * len, to - rightFrag * len);
    }
    final sink = StringBuffer(substring(from - leftFrag * len));
    _repeat(sink, this, fragOffset);
    sink.write(substring(0, to - rightFrag * len));
    return sink.toString();
  }

  String center(int width, {String fill = " ", Len lengthFunc}) {
    if (fill == null || fill.isEmpty) {
      throw ArgumentError("fill cannot be null or empty");
    }
    final len = lengthFunc == null ? length : lengthFunc(this);
    if (len >= width) {
      return this;
    }
    var input = this;
    final padding = width - len;
    if (padding ~/ 2 > 0) {
      input = fill.loop(0, padding ~/ 2) + input;
    }
    return input + fill.loop(lengthFunc == null ? input.length : lengthFunc(input) - width, 0, lengthFunc);
  }

  void _repeat(StringBuffer sink, String s, int times) {
    for (var i = 0; i < times; i++) {
      sink.write(s);
    }
  }
}

extension Capitalize on String {
  String capitalize() => "${this[0].toUpperCase()}${substring(1)}";
}

extension KeyName on Key {
  String get name =>
      (isControl ? controlChar.toString().split(".")[1] : char.toString())
          .capitalize();
}

String stitchStrings(List<String> strings, {Len lengthFunc}) {
  lengthFunc ??= (x) => x.length;
  var lines = strings.map((i) => i.split("\n"));
  final returnLines = <String>[];
  final boxWidths = lines.map((i) => i.map(lengthFunc).reduce(max)).toList();
  final maxBoxHeight = lines.map((i) => i.length).reduce(max);
  lines = lines.map((i) {
    i.length = maxBoxHeight;
    return i;
  });
  for (var i = 0; i < maxBoxHeight; i++) {
    returnLines.add("");
    var j = 0;
    for (final k in lines) {
      var s = k[i] ?? "";
      s += " " * (boxWidths[j] - lengthFunc(s));
      returnLines[i] += s;
      j++;
    }
  }

  return returnLines.join("\n");
}
