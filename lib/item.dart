import "const.dart";
import "container.dart";

/// The rarity level of an item.
// ignore: public_member_api_docs
enum Rarity { common, uncommon, rare, epic, legendary }

/// An transferable, usable or equipable instance that exists between
/// [Container] objects. Actually represents a stack of items with the [count]
/// attribute.
class Item {
  /// Constructs an [Item] from entirely optional parameters.
  Item(
      {this.inventory,
      this.name = "unnamed item",
      this.desc = "",
      this.rarity = Rarity.common,
      this.sg,
      this.pl,
      this.count = 1,
      this.stackSize = 1}) {
    /// Default singular of an item to be 'a [name]'
    sg ??= "a $name";

    /// Default plural of an item to be '[name]s'
    pl ??= "${name}s";
  }

  /// Clone an [Item] from a different [Item], as dart has no built-in deepcopy.
  Item.from(Item other) {
    inventory = other.inventory;
    // equipment = other.equipment;
    name = other.name;
    desc = other.desc;
    rarity = other.rarity;
    sg = other.sg;
    pl = other.pl;
    count = other.count;
    stackSize = other.stackSize;
  }

  /// The [Container] this is stored in.
  Container inventory;
  // Equipment equipment
  /// Name of this
  String name;

  /// Additional description of this.
  String desc;

  /// Rarity level of this.
  Rarity rarity;

  /// If irregular, the singular form of the [name].
  String sg;

  /// If irregular, the plural form of the [name].
  String pl;

  /// How much of the item is included in this stack.
  int count;

  /// How much of the item could be included in a stack.
  int stackSize;

  /// Whether or not the item stack is at full capacity.
  bool get filled => count >= stackSize;

  /// Whether or not there is only one item in this item stack.
  bool get single => count == 1;

  /// Name and rarity of the item, excluding count. [plural] specifies if the
  /// [sg] or [pl] property should be used.
  String toTypeString({bool plural = false}) => "${rarityColors[rarity]}${plural ? pl : sg}%XE ${rarityColors[rarity]}"
      "%SB[ ${rarityLetters[rarity]} ]%XE";

  /// Name, rarity and count of the item.
  @override
  String toString() => "${rarityColors[rarity]}%SI$name%XE${rarityColors[rarity]} "
      "%SB[ ${rarityLetters[rarity]} ] %XEx$count";

  @override
  bool operator ==(dynamic other) =>
      runtimeType == other.runtimeType &&
      name == other.name &&
      desc == other.desc &&
      rarity == other.rarity &&
      sg == other.sg &&
      pl == other.pl &&
      stackSize == other.stackSize;

  @override
  int get hashCode => toString().hashCode;
}
