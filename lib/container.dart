import "dart:math";

import "entity.dart";
import "item.dart";

/// Holds a list of [Item] objects in one way or another in [Container.items]
/// and allows for adding or removing them via [Container.add].
abstract class Container {
  /// Contents of the [Container].
  List<Item> items;

  /// Add the item [item] to the [Container].
  String add(Item item);

  @override
  String toString() => items.map((item) => item?.toString() ?? "%FREMPTY%XE").join(", ");
}

/// An entry for [LinearContainerAddResult]. Specifies how much of an item was
/// placed in what slot.
class LinearContainerAddEntry {
  /// A full entry containing [slot], [initialCount] and [actualCount].
  LinearContainerAddEntry(this.slot, this.initialCount, this.actualCount);

  /// A terminating entry (has no other content).
  LinearContainerAddEntry.terminate() {
    terminating = true;
  }

  /// The slot of a [LinearContainer] an amount of the relevant [Item] was added
  /// to.
  int slot;

  /// How much of the relevant [Item] was added before it ran out or filled up.
  int initialCount;

  /// How much of the relevant [Item] is now in the slot after adding.
  int actualCount;

  /// Whether or not this entry is what terminates the add result
  /// (content doesn't matter).
  bool terminating = false;
}

/// An object returned by the internal implementation of [LinearContainer.add],
/// that is then formatted to be human-readable.
class LinearContainerAddResult {
  /// Initialize empty result.
  LinearContainerAddResult();

  /// See [LinearContainerAddEntry].
  List<LinearContainerAddEntry> entries = [];

  /// A counter variable incremented by [roll()]. Mostly unused. Could be
  /// deleted.
  int count = 0;

  /// Creates a new [LinearContainerAddEntry] for [entries].
  void addEntry(int slot, int initialCount, int actualCount) =>
      entries.add(LinearContainerAddEntry(slot, initialCount, actualCount));

  /// Creates a terminating [LinearContainerAddEntry] for [entries] using
  /// [LinearContainerAddEntry.terminating()];
  void terminate() => entries.add(LinearContainerAddEntry.terminate());

  /// Increments [count].
  void roll() {
    count++;
  }

  /// Convert this into a human readable form, given the context [Item]
  /// [contextItem], which was added to the [LinearContainer].
  String parse(Item contextItem) {
    var analysis = <String>[];
    var summed = 0;
    if (entries.length == 1 && entries.single.terminating) {
      return "%FR%SBInventory is full!%XE";
    }
    for (final entry in entries) {
      if (entry.terminating) {
        analysis.add("%fRNo more could fit.%XE%");
      } else {
        final diff = entry.actualCount - entry.initialCount;
        final filled = entry.actualCount == contextItem.stackSize;
        final countColor = filled ? "%fR" : "%fG";
        final itemName =
        diff == 1 ? "${contextItem.toTypeString()}" : "%fG$diff ${contextItem.toTypeString(plural: true)}";
        analysis.add(
            "Added $itemName to slot %fP${entry.slot + 1}%XE (now ${countColor}x${entry
                .actualCount}%XE / %fCx${contextItem.stackSize}%XE)");
        summed += diff;
      }
    }
    if (analysis.length > 1) {
      analysis = analysis.map((var i) => "    $i").toList()
        ..insert(
            0,
            "Adding %fG$summed "
            "${contextItem.toTypeString(plural: true)}");
    }
    return analysis.join("\n");
  }
}

/// Holds its [Item] instances in a slot-based list with a fixed size
/// [LinearContainer.size].
abstract class LinearContainer extends Container {
  /// Creates a [LinearContainer] given the amount of slots.
  LinearContainer.fromSize(this.size) {
    items = List(size);
  }

  /// Amount of slots in this.
  int size;

  /// Internal implementation for adding items to the container. Only meant to
  /// be interfaced by [LinearContainer.add].
  ///
  /// The implementation is recursive. On the first time, only [item] is passed
  /// in to the function but if the item fits into the inventory only partially,
  /// a part of the item is split off and passed into the function again.
  /// In this case, the split off excess amount is placed into [item] and the
  /// original item with untouched quantity is instead in [originalItem]. An
  /// additional [snowball] parameter is also passed along in recursion, keeping
  /// track of where and when these splits took place. See more in
  /// [LinearContainerAddResult].
  LinearContainerAddResult rawAdd(Item item, [Item originalItem, LinearContainerAddResult snowball]) {
    originalItem ??= item;
    snowball ??= LinearContainerAddResult();
    snowball.roll();
    for (final lookForEmpty in [false, true]) {
      for (final entry in items
          .asMap()
          .entries) {
        final i = entry.key;
        var iterItem = entry.value;
        var initialCount = 0;
        var maxSize = item.stackSize;
        if (!lookForEmpty && (iterItem != null) && (item == iterItem)) {
          if (iterItem.count >= maxSize) {
            continue;
          }
          initialCount = iterItem.count;
          maxSize = iterItem.stackSize;
        } else if (lookForEmpty && (iterItem == null)) {
          iterItem = Item.from(item)
            ..count = initialCount
            ..inventory = this;
          items[i] = iterItem;
        } else {
          continue;
        }
        final newCount = initialCount + item.count;
        final overflow = max(0, newCount - maxSize);
        if (overflow == 0) {
          iterItem.count = newCount;
          originalItem = null;
          snowball.addEntry(i, initialCount, newCount);
          return snowball;
        } else {
          iterItem.count = maxSize;
          originalItem.count = overflow;
          snowball.addEntry(i, initialCount, iterItem.count);
          return rawAdd(Item.from(originalItem), originalItem, snowball);
        }
      }
    }
    snowball.terminate();
    return snowball;
  }

  /// Outward-facing implementation for adding items to the container, giving an
  /// user-readable output.
  @override
  String add(Item item) => rawAdd(item).parse(item);

  /// Lists all the items in this in a nice readable format.
  ///
  /// Additionaly, a function [test] can be specified, taking an [Item] as a
  /// parameter. If this function returns `false`, that item isn't inlcuded
  /// in the output. The parameter [includeEmpty] automatically doesn't include
  /// empty slots in [items].
  String listItems({bool Function(Item) test, bool includeEmpty = true}) {
    test ??= (_) => true;
    final returnList = <String>[];
    var merge = false;
    var mergeStart = 0;
    items.asMap().forEach((final slot, final item) {
      if ((item == null && !includeEmpty) || !test(item)) {
        return;
      }
      if (merge && (slot == size - 1 || items[slot + 1] != null)) {
        returnList.add("slot %fP${mergeStart + 1}-${slot + 1}%XE: ");
        merge = false;
      } else if (merge && item == null) {
        return;
      } else if (item == null && slot != size - 1 && items[slot + 1] == null) {
        mergeStart = slot;
        merge = true;
      } else {
        returnList.add("slot %fP${slot + 1}%XE: ");
      }
      if (!merge) {
        final filled = item?.filled ?? false;
        final single = item?.single ?? false;
        final itemName = "${filled ? '%fR' : '%fG'}"
            "${!single ? '${item?.count.toString()} ' : ''}"
            "${item?.toTypeString(plural: !single)}"
            "${!single ? (' out of ${filled ? '%fR' : '%fG'}${item?.stackSize.toString()}') : ''}%XE";
        returnList.last += item != null ? itemName : "%FREMPTY%XE";
      }
    });
    return returnList.join("\n");
  }

  @override
  String toString() => listItems();
}

/// Player inventories are instances of [LinearContainer] that also interface with the [PlayerEntity]
class Inventory extends LinearContainer {
  /// Inventory initializes exactly like [LinearContainer]
  Inventory.fromSize(size) : super.fromSize(size);
}

mixin ContainerHolder<T extends Container> {
  T inventory;

  void give(Item item) {
    inventory.add(item);
  }
}
