import "dart:convert";
import "dart:io";
import "dart:math";
import "dart:typed_data";

import "package:convert/convert.dart";

class ESBFileUnion {
  ESBFileUnion(this.header, this.content);

  String header;

  dynamic content;

  @override
  String toString() => "$header, $content";
}

class ArrayReadUnion {
  ArrayReadUnion(this.content, this.skip);

  dynamic content;

  int skip;
}

class ArrayStateUnion {
  ArrayStateUnion(this.load, this.mode);

  int load;

  ArrayReadMode mode;
}

enum ArrayReadMode { invalid, int, float, str, none }

String stringToBinary(String s) {
  final bytes = utf8.encode(s);
  return "${hex.encode(bytes)}00";
}

String binaryToString(List<int> bytes) => "${utf8.decode(bytes)}";

void encodeToFile(String filename, Map<String, dynamic> content, String header, {bool compress = true}) {
  /*var bytes = hex.decode(stringToBinary(header) + convertMap(content));
  if (compress) {
    bytes = zlib.encoder.convert(bytes);
  }
  File(filename).writeAsBytesSync(bytes);*/
  throw UnimplementedError();
}

String identify<T extends dynamic>(T x, {bool includeTypes = true}) {
  var y = x;
  if (x is bool) {
    y = (x ? 1 : 0) as dynamic;
  }
  if (y is String) {
    return (includeTypes ? "07" : "") + stringToBinary(y);
  } else if (y is int) {
    final yType = esbTypeof(y);
    final negativeMask = pow(256, yType) - 1;
    final yRadix = (y & negativeMask).toRadixString(16);
    return (includeTypes ? "0$yType" : "") + yRadix.padLeft(pow(2, yType), "0");
  } else if (y is double) {
    final yBytes = ByteData(8)..setFloat64(0, y);
    final buffer = StringBuffer();
    for (final i in yBytes.buffer.asUint8List()) {
      buffer.write(i.toRadixString(16).padLeft(2, "0"));
    }
    return (includeTypes ? "06" : "") + buffer.toString();
  } else if (y is Map<String, dynamic>) {
    return convertMap(y, includeTypes: includeTypes);
  } else if (y is Iterable<dynamic>) {
    if (y.every((i) {
      final j = esbTypeof(i);
      return j > -1 && j == esbTypeof(y.first);
    })) {
      return (includeTypes ? (esbTypeof(y.first) + 8).toRadixString(16).padLeft(2, "0") : "") +
          convertList(y as dynamic, includeTypes: false);
    }
    return (includeTypes ? "10" : "") + convertList(y as dynamic);
  } else if (y == null) {
    return "ff";
  } else {
    throw ArgumentError.value(y, null, "Can't encode into ESB");
  }
}

String convertMap(Map<String, dynamic> data, {bool includeTypes = false}) {
  final buffer = StringBuffer(includeTypes ? "08" : "");
  data.forEach((key, value) {
    buffer..write(stringToBinary(key))..write(identify(value));
  });
  return (buffer..write("00")).toString();
}

String convertList(Iterable data, {bool includeTypes = true}) {
  final buffer = StringBuffer();
  data.forEach((element) {
    buffer.write(identify(element, includeTypes: includeTypes));
  });
  return (buffer..write("00")).toString();
}

int esbTypeof(dynamic x) {
  if (x is int) {
    return (((-x - 1).bitLength + 1) / 8).ceil();
  } else if (x is double) {
    return 6;
  } else if (x is String) {
    return 7;
  }
  return -1;
}

ESBFileUnion decodeFromFile(String filename) => convertFromEsb(zlib.decoder.convert(File(filename).readAsBytesSync()));

ESBFileUnion convertFromEsb(Iterable<int> bytes) {
  final headerBytes = bytes.takeWhile((i) => i > 0);
  final header = utf8.decode(headerBytes.toList());
  final content = getArray(bytes.skip(headerBytes.length + 1).toList()).content;
  return ESBFileUnion(header, content);
}

ArrayReadUnion getArray(List<int> bytes, {int presetType, bool isNamed = true}) {
  final presetState = fromESBTypes[presetType];
  final dynamic output = isNamed ? <String, dynamic>{} : [];
  final buffer = <int>[];
  dynamic val;
  var i = -1;
  var load = isNamed ? -1 : presetState == null ? 0 : presetState.load;
  var mode = presetState == null ? ArrayReadMode.invalid : presetState.mode;
  int char;
  String name;

  for (;;) {
    i++;
    char = bytes[i];

    if (load > 0) {
      load--;
      buffer.add(char);
      if (mode.index > 0 && load > 0) {
        continue;
      }
    } else if (load == -1) {
      if (char > 0) {
        buffer.add(char);
        if (mode.index > 0) {
          continue;
        }
      } else {
        load = 0;
      }
      if (mode == ArrayReadMode.invalid) {
        continue;
      }
    }

    if (mode == ArrayReadMode.invalid) {
      if (isNamed) {
        name = binaryToString(buffer);
      }
      buffer.clear();
      if (fromESBFunctions.containsKey(char)) {
        final out = fromESBFunctions[char](bytes.skip(i + 1).toList());
        val = out.content;
        i += out.skip + 2;
      } else {
        final out = fromESBTypes[char];
        load = out.load;
        mode = out.mode;
        if (mode != ArrayReadMode.none) {
          continue;
        }
      }
    }
    if (mode != ArrayReadMode.invalid) {
      if (isNamed) {
        output[name] = fromESBConversions[mode](buffer);
      } else {
        output.add(fromESBConversions[mode](buffer));
      }
      buffer.clear();
    }

    if (val != null) {
      if (isNamed) {
        output[name] = val;
      } else {
        output.add(val);
      }
    }

    if (mode != ArrayReadMode.invalid || val != null) {
      if (bytes[i + 1] == 0) {
        break;
      }
      val = null;
      load = isNamed ? -1 : presetState == null ? 0 : presetState.load;
      mode = presetState == null ? ArrayReadMode.invalid : presetState.mode;
    }
  }

  return ArrayReadUnion(output, i);
}

final Map<int, ArrayStateUnion> fromESBTypes = {
  0x01: ArrayStateUnion(1, ArrayReadMode.int),
  0x02: ArrayStateUnion(2, ArrayReadMode.int),
  0x03: ArrayStateUnion(4, ArrayReadMode.int),
  0x04: ArrayStateUnion(8, ArrayReadMode.int),
  0x06: ArrayStateUnion(8, ArrayReadMode.float),
  0x07: ArrayStateUnion(-1, ArrayReadMode.str),
  0xff: ArrayStateUnion(0, ArrayReadMode.none),
};

final Map<int, ArrayReadUnion Function(List<int>)> fromESBFunctions = {
  0x08: getArray,
  0x10: (x) => getArray(x, isNamed: false),
  0x09: (x) => getArray(x, isNamed: false, presetType: 0x1),
  0x0a: (x) => getArray(x, isNamed: false, presetType: 0x2),
  0x0b: (x) => getArray(x, isNamed: false, presetType: 0x3),
  0x0c: (x) => getArray(x, isNamed: false, presetType: 0x4),
  0x0d: (x) => getArray(x, isNamed: false, presetType: 0x5),
  0x0e: (x) => getArray(x, isNamed: false, presetType: 0x6),
  0x0f: (x) => getArray(x, isNamed: false, presetType: 0x7),
};

final Map<ArrayReadMode, dynamic Function(List<int>)> fromESBConversions = {
  ArrayReadMode.str: binaryToString,
  ArrayReadMode.int: (x) {
    var single = int.parse(hex.encode(x), radix: 16);
    if ((single & ((pow(256, x.length)) ~/ 2)) > 0) {
      single = (~single & (pow(256, x.length) - 1)) + 1;
      single = -single;
    }
    return single;
  },
  ArrayReadMode.float: (x) {
    final bytes = ByteData(8);
    x.asMap().forEach(bytes.setUint8);
    return bytes.getFloat64(0);
  },
  ArrayReadMode.none: (_) => null,
};
