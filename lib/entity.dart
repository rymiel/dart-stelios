import "const.dart";
import "container.dart";
import "randomchoice.dart";
import "sources.dart";

final dynamic tempEntity = {
  "puma": {
    "name": "Puma",
    "lv": [3, 5, 8, 1],
    "attrs": {
      "vit": [6, 9, 21, 25, 35],
      "int": [0],
      "dex": [17, 23, 40, 80, 100],
      "str": [11, 15, 23, 60, 50],
      "wil": [4, 7, 11, 65, 15],
      "lck": [4]
    }
  }
};

class _StatVal {
  _StatVal([this._val = 0, this._maxVal = 999]);

  num _val;
  final int _maxVal;
  int get val => _val.toInt().clamp(0, _maxVal);
  set val(num newVal) => _val = newVal.toInt().clamp(0, _maxVal);

  _StatVal operator +(int other) => _StatVal(val + other);
  _StatVal operator -(int other) => _StatVal(val - other);

  @override
  String toString() => "$val";
}

class StatRange {
  StatRange(List<num> vals) {
    if (vals.length == 4) {
      min = vals[0];
      val = vals[1];
      max = vals[2];
      preferenceDec = preferenceInc = vals[3];
    } else if (vals.length == 5) {
      min = vals[0];
      val = vals[1];
      max = vals[2];
      preferenceDec = vals[3];
      preferenceInc = vals[4];
    } else if (vals.length == 1) {
      min = max = val = vals.single;
      preferenceDec = preferenceInc = 0;
    }
  }

  StatRange.deviant(List<num> vals) {
    min = vals[0];
    val = vals[1];
    max = vals[2];
    deviance = vals[3];
  }

  num val, min, max, preferenceDec, preferenceInc, deviance;

  num preference(int sign) => sign > 0 ? preferenceInc : preferenceDec;

  int get random => standardDeviation(val, deviance).clamp(min, max).toInt();

  @override
  String toString() => "$val ($min-$max, $preference%)";
}

/// A "stat dictionary", keeps track of the stats of an [Entity].
class Stat {
  Stat(Map<String, int> intVals) {
    vals = intVals.map(cast);
  }

  /// Creates a stat dictionary with the keys from [allStats] and 0 value.
  Stat.blank() {
    vals = Map<String, int>.fromIterable(allStats, value: (_) => 0).map(cast);
  }

  // Stat._ignore();

  /// Stores the stat values of this stat dictionary.
  Map<String, _StatVal> vals = {};

  /// "Casts" a [key] and its [val] into a [MapEntry] containing the same [key]
  /// but the [val] converted into an internal statval object, required for
  /// storing into the [vals] of this.
  static MapEntry<String, _StatVal> cast(String key, num val) => MapEntry(key, _StatVal(val));

  /// Fetch a value from this stat dictionary, converting the internal object
  /// into an int. Defaults to 0 if stat is absent.
  int operator [](String key) => vals[key]?.val ?? 0;

  /// Sets a stat of the stat dictionary, converting an int to the internal stat
  /// value object.
  void operator []=(String key, int val) => vals[key] = _StatVal(val);
}

/// A special case of [Stat] that has some additional special stats that are
/// calculated from existing stats when using the `[]` operator.
class RawStat extends Stat {
  /// Same functionality.
  RawStat.blank() : super.blank();

  @override
  int operator [](String key) {
    switch (key) {
      case "per":
        return _StatVal(this["int"] * 2 + this["lck"] / 2 + this["dex"]).val;
      case "ini":
        return _StatVal(this["dex"] + this["int"] / 1.5).val;
      default:
        return super[key];
    }
  }
}

class RangeStat {
  RangeStat(Map<String, List<int>> intVals) {
    vals = intVals.map(cast);
  }

  Map<String, StatRange> vals = {};

  static MapEntry<String, StatRange> cast(String key, List<int> val) => MapEntry(key, StatRange(val));

  void fudge(int amount) {
    if (amount == 0) {
      return;
    }
    final change = amount > 0 ? 1 : -1;
    while (amount != 0) {
      final choices = Map.fromEntries(vals.entries.where((i) =>
      i.value.preference(change) > 0 &&
          ((change == 1 && i.value.val < i.value.max) || (change == -1 && i.value.val > i.value.min))));
      if (choices.isEmpty) {
        return;
      }
      final stats = choices.keys;
      final weights = choices.values.map((i) => i.preference(change).toDouble());
      final toFudge = randomChoice(stats, weights);
      vals[toFudge].val += change;
      amount -= change;
    }
  }

  Map<String, E> asMap<E>(MapEntry<String, E> Function(String, num) func) {
    func ??= (i, j) => MapEntry(i, j as dynamic);
    return Map.fromIterables(vals.keys, vals.values.map((i) => i.val)).map(func);
  }
}

/// Combines two [Stat] objects, [statBase] and [statVolatile]. When accessing
/// a stat value using the `[]` operator, combines both. When setting a stat
/// value using the `[]=` operator, only sets [statBase].
class StatUnion {
  /// Combines [statBase] and [statVolatile].
  StatUnion(this.statBase, this.statVolatile);

  /// Modified by default.
  Stat statBase;

  /// Additional.
  Stat statVolatile;

  /// Getting combines both.
  int operator [](String key) => statBase[key] + statVolatile[key];

  /// Setting sets [statBase].
  void operator []=(String key, int val) => statBase[key] = val;
}

/// Has a name, stats, inventory, skills and status. Most interactive things
// are entities, including player characters, which are instances of
/// [PlayerEntity].
class Entity with ContainerHolder<Inventory> {
  /// Creates an [Entity] with blank stats and a given [name].
  Entity([this.name = "Unnamed entity"]) {
    inventory = Inventory.fromSize(5);
    stat = StatUnion(staticStat, modStat);
  }

  factory Entity.fromID(String id) => schemata.entitySchema.generate(id);

  /// Name of the entity.
  String name;

  /// Static, [RawStat] of this.
  RawStat staticStat = RawStat.blank();

  /// Volatile, modification [Stat] of this.
  Stat modStat = Stat.blank();

  /// Union of [staticStat] and [modStat].
  StatUnion stat;

  // Room inside
  // Battle battle
  // Equipment equipment
  // List<Effect> effects
  // List<Skill> skills

  @override
  String toString() {
    var levelText = "";
    if (stat["lv"] > 0) {
      levelText = " (LV ${stat['lv']})";
    }
    return "%SB$name%SE$levelText";
  }
}

/// An [Entity] that also interfaces with the player.
class PlayerEntity extends Entity {
  /// Set a level for player character so that it will show up.
  PlayerEntity([name]) : super(name) {
    stat["lv"] = 1;
  }
}
