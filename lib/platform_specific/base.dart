// ignore: implementation_imports
import "package:dart_console/src/key.dart";
import "package:piecemeal/piecemeal.dart";

abstract class ConsoleUnion {
  Vec get cursorPosition;

  set cursorPosition(Vec cursorPosition);

  void write(String text);

  void clearScreen();

  void init();

  void cleanUp();

  int get windowWidth;

  int get windowHeight;

  Future<Key> readKey();

  void resetColor();

  void exit(int code);

  void simpleMode();
}

class Console extends ConsoleUnion {
  @override
  Vec cursorPosition;

  @override
  void cleanUp() {
    throw UnimplementedError();
  }

  @override
  void clearScreen() {
    throw UnimplementedError();
  }

  @override
  void init() {
    throw UnimplementedError();
  }

  @override
  Future<Key> readKey() async {
    throw UnimplementedError();
  }

  @override
  void resetColor() {
    throw UnimplementedError();
  }

  @override
  int get windowHeight => throw UnimplementedError();

  @override
  int get windowWidth => throw UnimplementedError();

  @override
  void write(String text) {
    throw UnimplementedError();
  }

  @override
  void exit(int code) {
    throw UnimplementedError();
  }

  @override
  void simpleMode() {
    throw UnimplementedError();
  }
}

Future<void> prepareSchemata() async {
  throw UnimplementedError();
}