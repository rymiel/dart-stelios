import "dart:async";
import "dart:convert";
import "dart:html";

import "package:dart_console/src/key.dart";
import "package:http/http.dart" as http;
import "package:malison/malison.dart";
import "package:malison/malison_web.dart";
import "package:piecemeal/piecemeal.dart";

import "../const.dart";
import "../sources.dart";
import "base.dart" show ConsoleUnion;

class Console extends ConsoleUnion {
  CanvasTerminal terminal;
  WebScreen screen;

  String foreMemory;
  String backMemory;
  String styleMemory;

  Color get fore => malisonColorConversion[styleMemory == "B" ? foreMemory.toUpperCase() : foreMemory];

  Color get back => malisonColorConversion[backMemory];

  static const malisonColorConversion = {
    "x": Color.black,
    "r": Color.red,
    "g": Color.green,
    "y": Color.yellow,
    "b": Color.blue,
    "p": Color.purple,
    "c": Color.aqua,
    "w": Color.lightGray,
    "X": Color.darkGray,
    "R": Color.lightRed,
    "G": Color.lightGreen,
    "Y": Color.lightYellow,
    "B": Color.lightBlue,
    "P": Color.lightPurple,
    "C": Color.lightAqua,
    "W": Color.white,
  };

  bool doColor = true;

  @override
  Vec cursorPosition = Vec.zero;

  @override
  void clearScreen() {
    terminal.clear();
  }

  @override
  void init() {
    resetColor();
    final canvas = CanvasElement();
    document.body.children.add(canvas);
    terminal = CanvasTerminal(windowWidth, windowHeight, Font("Consolas", size: 12, w: 8, h: 14, x: 1, y: 11), canvas);

    screen = WebScreen();
    UserInterface<String>(terminal).push(screen);
  }

  @override
  void cleanUp() {}

  @override
  int get windowWidth => 120;

  @override
  int get windowHeight => 40;

  @override
  void write(String text) {
    final colorRuns = <String>[];
    final colorCodes = <String>[];
    text.splitMapJoin(colorCodeRegex, onMatch: (m) {
      colorCodes.add(m[0].substring(1));
      return "";
    }, onNonMatch: (m) {
      colorRuns.add(m);
      return "";
    });
    var localOffset = 0;
    for (var i = 0; i < colorRuns.length; i++) {
      final s = colorRuns[i];
      if (i > 0) {
        final c = colorCodes[i - 1];
        if (c[0] == "F") {
          foreMemory = c[1].toUpperCase();
        } else if (c[0] == "f") {
          foreMemory = c[1].toLowerCase();
        } else if (c[0] == "B") {
          backMemory = c[1].toUpperCase();
        } else if (c[0] == "b") {
          backMemory = c[1].toLowerCase();
        } else if (c[0] == "S") {
          styleMemory = c[1];
        } else if (c == "XE") {
          resetColor();
        }
      }
      if (s.isNotEmpty) {
        Function() c(row, col, t, f, b) => () {
          if (doColor) {
            terminal
              ..writeAt(row, col, t, f, b)
              ..render();
          } else {
            terminal
              ..writeAt(row, col, t)
              ..render();
          }
        };

        Future.delayed(Duration.zero,
            c(cursorPosition.y + localOffset, cursorPosition.x, s, fore, back));

        localOffset += s.length;
      }
    }
  }

  @override
  Future<Key> readKey() {
    final _key = Completer<Key>();
    screen.ui.handlingInput = true;
    screen.keyHandler = (key) {
      screen
        ..keyHandler = null
        ..ui.handlingInput = false;
      _key.complete(key);
    };
    return _key.future;
  }

  @override
  void resetColor() {
    foreMemory = "w";
    backMemory = "x";
    styleMemory = "E";
  }

  @override
  void exit(int code) {
    // TODO: implement exit
  }

  @override
  void simpleMode() {
    doColor = false;
  }
}

class WebScreen extends Screen<String> {
  void Function(Key) keyHandler;

  @override
  bool keyDown(int keyCode, {bool shift, bool alt}) {
    if (keyCode == KeyCode.enter) {
      keyHandler(Key.control(ControlCharacter.enter));
    }
    return false;
  }
}

Future<void> prepareSchemata() async {
  final data = jsonDecode((await http.get("sources.json")).body);
  schemata = Schemata.fromESB(data["content"]);
}
