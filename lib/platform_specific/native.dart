import "dart:io" as io;

import "package:dart_console/dart_console.dart" as native;
import "package:dart_console/src/ansi.dart";
import "package:piecemeal/piecemeal.dart";

import "../const.dart";
import "../esb.dart";
import "../sources.dart";
import "base.dart" show ConsoleUnion;

class Console extends ConsoleUnion {
  final native.Console console = native.Console();
  String Function(String text) colorConversion = cconv;

  /// Converts color escape sequences to ANSI SGR codes.
  static String cconv(String text) => text.replaceAllMapped(
      colorCodeRegex, (m) => ansiSetColor(cLookup(m[1], m[2])));

  @override
  void clearScreen() {
    console.clearScreen();
  }

  @override
  void init() {
    console
      ..resetColorAttributes()
      ..rawMode = true
      ..hideCursor();
  }

  @override
  void cleanUp() {
    console
      ..resetColorAttributes()
      ..rawMode = false
      ..showCursor();
  }

  @override
  int get windowWidth => console.windowWidth;

  @override
  int get windowHeight => console.windowHeight;

  @override
  void write(String text) {
    console.write(colorConversion(text));
  }

  @override
  Future<native.Key> readKey() async {
    final key = console.readKey();
    console.rawMode = true;
    return key;
  }

  @override
  void resetColor() {
    console.write(ansiResetColor);
  }

  @override
  set cursorPosition(Vec pos) {
    console.cursorPosition = native.Coordinate(pos.x, pos.y);
  }

  @override
  Vec get cursorPosition =>
      Vec(console.cursorPosition.row, console.cursorPosition.col);

  @override
  void exit(int code) {
    io.exit(code);
  }

  @override
  void simpleMode() {
    colorConversion = (i) => i;
  }
}

Future<void> prepareSchemata() async {
  schemata = Schemata.fromESB(decodeFromFile("lib/packed/sources.esb").content);
}
