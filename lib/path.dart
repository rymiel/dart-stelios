import "battle.dart";
import "entity.dart";
import "event.dart";
import "party.dart";

void encounterBattle(Context ctx, List<String> encounters) {
  if (encounters.length != 10) {
    throw ArgumentError.value(encounters, "encounters",
        "regular encounters must have 10 enemy entities to form a party");
  }
  final enemyEncounter = Formation.fromArray(
      encounters.map((e) => e == null ? null : Entity.fromID(e)).toList());
  Battle(ctx.room, enemyEncounter);
}

class Room extends RoomSchematic with EventSubject {
  Room.fromSchematic(RoomSchematic schematic) : super.blank() {
    onCreate = schematic.onCreate;
  }

  List<Entity> contents = [];
  Party playerParty;

  @override
  List<EventSubject> get passDown => [];

  @override
  Context resolve(String eventName, ContextBuilder ctx) {
    switch (eventName) {
      case "onRoomCreate":
        return ctx(onCreate)..room = this;
      case "someOtherEvent":
        return ctx(onCreate);
      default:
        return null;
    }
  }
}

class RoomSchematic {
  RoomSchematic(this.onCreate);

  RoomSchematic.blank();

  Event onCreate;
}

class RoomUnit {
  RoomUnit(this._room, this.difficulty, this.rarity);

  final RoomSchematic _room;
  Room get room => Room.fromSchematic(_room);
  int difficulty;
  int rarity;
}

class Path {
  Path(this.name, this.possibleRooms);

  List<RoomUnit> possibleRooms;
  int distance = 1;
  final String name;

  Room get randomRoom => Room.fromSchematic((possibleRooms..shuffle()).first.room);

  Room generateRoomFor(Party playerParty) {
    // playerParty.screen.write(...)
    final newRoom = randomRoom
      ..playerParty = playerParty;
    playerParty.raise("onRoomCreate", newRoom);
    return newRoom;
  }
}
