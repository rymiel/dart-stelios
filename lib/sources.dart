import "entity.dart";
// import "esb.dart";

abstract class Schema<T> {
  Schema(this.items);

  T generate(String identifier);

  final Map<dynamic, dynamic> items;
}

class EntitySchema implements Schema<Entity> {
  EntitySchema(this.items);

  @override
  final Map<String, dynamic> items;

  @override
  Entity generate(String id) {
    final entityTemplate = items[id];
    final newEntity = Entity(entityTemplate["name"]);
    final lv = StatRange.deviant(items[id]["lv"].cast<int>());
    newEntity.staticStat["lv"] = lv.random;
    final int fudge = newEntity.staticStat["lv"] - lv.val;
    // FIXME: is it possible to reduce the amount of generics here?
    newEntity.staticStat.vals.addAll(
        (RangeStat(items[id]["attrs"].map<String, List<int>>((k, v) => MapEntry<String, List<int>>(k, v.cast<int>())))
          ..fudge(fudge * 10))
            .asMap(Stat.cast));
    return newEntity;
  }
}

class Schemata {
  Schemata();

  Schemata.fromESB(Map<String, dynamic> data) {
    schemata["entity"] = EntitySchema(data["entity"]);
  }

  final Map<String, Schema> schemata = {};

  EntitySchema get entitySchema => schemata["entity"];
}

Schemata schemata = Schemata();
