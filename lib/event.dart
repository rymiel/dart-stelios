import "path.dart";

class InvalidContextAccess implements Exception {
  InvalidContextAccess(this.tried, this.available);

  dynamic tried;
  Iterable<dynamic> available;

  @override
  String toString() => "Tried accessing ${tried.toString()} from event context but it was never set";
}

class Event implements Function {
  Event(this.func, [args, kwargs]) {
    this.args = args ?? [];
    this.kwargs = kwargs ?? {};
    ctx = Context();
  }

  Function func;
  List<dynamic> args;
  Map<Symbol, dynamic> kwargs;
  Context _ctx;

  Context get ctx => _ctx;

  set ctx(Context ctx) {
    _ctx = ctx;
    ctx.forEvent = this;
  }

  dynamic call() => Function.apply(func, [ctx, ...args], kwargs);
}

mixin EventSubject {
  Context resolve(String eventName, ContextBuilder ctx);

  List<EventSubject> get passDown;

  List<Event> resolveAndRaise(String eventName, Context passedContext) {
    final builder = _contextBuilder(passedContext);
    final resolvedEvent = resolve(eventName, builder).forEvent;
    final resolvedEvents = <Event>[];
    if (resolvedEvent != null) {
      resolvedEvents.add(resolvedEvent);
    }
    passDown.forEach((i) => resolvedEvents.addAll(i.resolveAndRaise(eventName, resolvedEvent.ctx)));
    return resolvedEvents;
  }
}

class Context {
  final _fields = ContextField<String, dynamic>();
  Event forEvent;

  Room get room => _fields["room"];

  set room(Room room) {
    _fields["room"] = room;
  }

  Room get otherRoom => _fields["otherRoom"];

  set otherRoom(Room room) {
    _fields["otherRoom"] = room;
  }

  @override
  String toString() => _fields._fields.toString();
}

class ContextField<K, V> {
  final Map<K, V> _fields = <K, V>{};

  V operator [](K key) {
    final value = _fields[key];
    if (value == null) {
      throw InvalidContextAccess(key, _fields.keys);
    }
    return value;
  }

  void operator []=(K key, V value) {
    _fields[key] = value;
  }
}

typedef ContextBuilder = Context Function(Event);

ContextBuilder _contextBuilder(Context forContext) => (applyTo) => applyTo.ctx = forContext;
