import "package:normal/normal.dart";
import "package:universal_platform/universal_platform.dart";

import "item.dart";

/// OMEGA -> ZETA -> BETA -> ALPHA -> RELEASE
const String longProtocol = "OMEGA";

/// w -> z -> b -> a -> r
const String shortProtocol = "w";

/// MAJOR version of semantic versioning.
const int majorVersion = 0;

/// MINOR version of semantic versioning.
const int minorVersion = 1;

/// PATCH version of semantic versioning.
const int patchVersion = 3;

/// Additional build information.
const String buildSuffix = "";

const String commitSuffix = String.fromEnvironment("commit");

/// Build + commit.
const String patchSuffix =
    "${buildSuffix != '' ? '+$buildSuffix' : ''}${commitSuffix != '' ? '-$commitSuffix' : ''}";

/// Semantic versioning in the format of MAJOR.MINOR.PATCH(+OTHER)
const String versionDotNotation = "$majorVersion.$minorVersion.$patchVersion"
    "$patchSuffix";

/// Short version name including DELTA for dart and protocol name.
final String versionLong = "DELTA $longPlatform $longProtocol $versionDotNotation";

/// Short version name including d for dart and protocol initial.
final String versionShort = "d$shortPlatform$shortProtocol$versionDotNotation";

final String longPlatform = UniversalPlatform.isWeb
    ? "CHI"
    : UniversalPlatform.isWindows ? "IOTA" : UniversalPlatform.isLinux ? "LAMBDA" : "QUESTION";

final String shortPlatform =
    UniversalPlatform.isWeb ? "x" : UniversalPlatform.isWindows ? "i" : UniversalPlatform.isLinux ? "l" : "q";

/// Matches color escape sequences.
final RegExp colorCodeRegex = RegExp(r"%([fFbBSX])([XRGYBPCWUEIF])");

const Map<String, List<int>> colorCodeLookup = {
  "X": [30, 90, 40, 100, 0, 0],
  "R": [31, 91, 41, 101, 0, 0],
  "G": [32, 92, 42, 102, 0, 0],
  "Y": [33, 93, 43, 103, 0, 0],
  "B": [34, 94, 44, 104, 1, 0],
  "P": [35, 95, 45, 105, 5, 0],
  "C": [36, 96, 46, 106, 0, 0],
  "W": [37, 97, 47, 107, 0, 0],
  "U": [0, 0, 0, 0, 4, 0],
  "E": [0, 0, 0, 0, 22, 0],
  "I": [0, 0, 0, 0, 7, 0],
  "F": [0, 0, 0, 0, 2, 0],
};
const List<String> _colorTypeOrder = ["f", "F", "b", "B", "S", "X"];

/// Convert the type char and color char to a ANSI SGR parameter value.
int cLookup(String type, String color) => colorCodeLookup[color][_colorTypeOrder.indexOf(type)];

/// List of stats included in the static stats of an entity.
const List<String> allStats = <String>[
  "points",
  "lv",
  "hp",
  "maxhp",
  "mp",
  "maxmp",
  "ap",
  "maxap",
  "vit",
  "int",
  "dex",
  "str",
  "wil",
  "lck",
  "threat"
];

/// Color codes associated with each rarity.
const Map<Rarity, String> rarityColors = <Rarity, String>{
  Rarity.common: "%FW", // Vibrant white
  Rarity.uncommon: "%FY", // Vibrant yellow
  Rarity.rare: "%FG", // Vibrant green
  Rarity.epic: "%FC", // Vibrant cyan
  Rarity.legendary: "%FR", // Vibrant red
};

/// Initials of each rarity level.
const Map<Rarity, String> rarityLetters = <Rarity, String>{
  Rarity.common: "C",
  Rarity.uncommon: "U",
  Rarity.rare: "R",
  Rarity.epic: "E",
  Rarity.legendary: "L",
};

num standardDeviation(num mu, num sigma) => Normal.generate(1, mean: mu, variance: sigma * sigma).single;
