// ignore: implementation_imports
import "package:dart_console/src/key.dart";

import "container.dart";
import "entity.dart";
import "event.dart";
import "ext.dart" show CenterText;
import "screen.dart";

/// Represents the location of the [Party] in the world.
class Location {
  /// Create a location based on [id] and [name]
  Location(this.id, this.name);

  /// Place name of ths.
  final String name;

  /// ID this was initialized with, used to determine equality.
  final String id;

  /// If the [id] pf two [Location] objects are the same, they are the same
  /// place in the world.
  @override
  bool operator ==(dynamic other) => runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;
}

/// Contains the [PlayerEntity] characters the player controls and also manages
/// other high level stuff, which means it also keeps track of the [screen]
/// object.
/// All members of [players] must share a single [inventory].
class Party with ContainerHolder<Inventory> {
  /// Create a party based off the [screen] object and a list of the
  /// [PlayerEntity] characters to add.
  Party(this.screen, Map<PlayerEntity, FormationPos> toAdd) {
    inventory = Inventory.fromSize(20);
    final unique = toAdd.values.toSet().toList().length == toAdd.values.length;
    if (!unique) {
      throw ArgumentError.value(toAdd.values.toString(), "toAdd", "Contains duplicate positions");
    }
    toAdd.forEach(addPlayer);
  }

  /// screen object.
  Screen screen;

  final List<PlayerEntity> _players = [];

  /// Characters the player controls. It may be undesirable to access that directly from here,
  /// as the [formation] provides information with better context. The same info may be accessed with
  /// [Formation.all].
  List<PlayerEntity> get players => List.unmodifiable(_players);

  Formation<PlayerEntity> formation = Formation();

  /// Current location of the party.
  Location location;

  void raise(String eventName, EventSubject applyTo) {
    applyTo.resolveAndRaise(eventName, Context()).forEach((i) {
      try {
        i();
      } on InvalidContextAccess catch (e, s) {
        final fullLog = "$s\nFields available: ${e.available.join(', ')}; but not ${e.tried}.\n";
        screen.renderDialogBox(
            title: "Non-fatal ${e.runtimeType.toString()} error",
            content: "$e\n\nThe game can attempt to recover from this, however some things may end up broken.\n"
                "Report the full log please.",
            optionNames: ["Continue despite error", "Read full log and exit", "Exit game"],
            optionKeys: [Key.control(ControlCharacter.enter), Key.printable("r"), Key.printable("x")],
            optionFunctions: [null, (t) => t.displayAndExit(fullLog), (t) => t.quit(1)]);
      }
    });
  }

  /// Adds the [PlayerEntity] [add] to the [players] list and makes their
  /// inventory [inventory].
  bool addPlayer(PlayerEntity add, FormationPos toPosition) {
    final successful = formation.addAt(add, toPosition);
    if (!successful) {
      return false;
    }
    _players.add(add);
    add.inventory = inventory;
    return true;
  }

  @override
  String toString() => players.join(", ");
}

class Formation<E extends Entity> {
  Formation({this.width = 3, this.lines = 3}) {
    formation = List.generate(lines, (_) => List(width), growable: false);
  }

  factory Formation.fromArray(List<Entity> entities, {width = 3, lines = 3}) {
    assert(entities.length == 1 + (width * lines),
        "Wrong number of entities for formation of this size");
    final f = Formation(width: width, lines: lines);
    for (var i = 0; i < entities.length; ++i) {
      final o = entities[i];
      if (i == 0) {
        f.hero = o;
      } else {
        final row = (i - 1) ~/ width;
        final index = (i - 1) % width;
        f.formation[row][index] = o;
      }
    }
    return f;
  }

  int width;
  int lines;
  List<List<E>> formation;

  E hero;

  bool addAt(E entity, FormationPos pos) {
    if (pos.totalRow != lines || pos.totalColumn != width) {
      // CONSIDER: alleviating this restriction
      throw UnsupportedError("$pos is describing a position not compatible with this formation");
    }

    if (pos.isHero && hero == null) {
      hero = entity;
      return true;
    } else if (formation[pos.row][pos.column] == null) {
      formation[pos.row][pos.column] = entity;
      return true;
    } else {
      return false;
    }
  }

  /// Hero facing right - vertical: false, flipped: false (default)
  /// Hero facing up - vertical: true, flipped: false
  /// Hero facing left - vertical: false, flipped: true
  /// Hero facing down - vertical: true, flipped: true
  String describe(int maxWidth, int maxHeight,
      {bool vertical = false, bool flipped = false, String divider = ":"}) {
    final horizontalCells = vertical ? width : lines + 1;
    final verticalCells = vertical ? lines + 1 : width;
    final horizontalPerCell =
        (maxWidth - (horizontalCells - 1)) ~/ horizontalCells;
    final verticalPerCell = maxHeight ~/ verticalCells;
    if (verticalPerCell < 1) {
      throw ArgumentError(
          "Can't fit formation information within $maxWidth x $maxHeight");
    }

    final stringFormation = <List<String>>[];

    for (var i = 0; i < width; i++) {
      final stringLine = <String>[];
      for (var j = lines - 1; j >= 0; j--) {
        final member = formation[j][i];
        stringLine.add(member == null ? "%SF%FXX%XE" : "%FW${member.name}%XE");
      }
      stringFormation.add(stringLine);
    }

    final directionArrow = vertical ? flipped ? "V" : "^" : flipped ? "<" : ">";
    var heroIndex = width ~/ 2;
    var formationTable = (stringFormation
          ..forEach((i) {
            i.add(heroIndex-- == 0
                ? hero != null
                    ? "%SB%FP<${hero.name}>%XE"
                    : "%SF%fP$directionArrow%XE"
                : "");
          }))
        .map((i) => i.map((j) => Canvas.ctruncate(j, horizontalPerCell)
            .center(horizontalPerCell, lengthFunc: Canvas.clength)));
    if (flipped) {
      formationTable = formationTable.map((i) => i.toList().reversed);
    }
    return formationTable.map((i) => i.join(divider)).join("\n");
  }

  List<E> get all {
    final build = <E>[];
    formation.forEach(build.addAll);
    build.insert(0, hero);

    return build;
  }
}

class FormationPos {
  FormationPos(this.row, this.column, {this.totalRow = 3, this.totalColumn = 3});

  FormationPos.hero({this.totalRow = 3, this.totalColumn = 3}) : isHero = true;

  int totalRow;
  int totalColumn;
  int row;
  int column;
  bool isHero = false;

  @override
  String toString() => "(row $row, column $column)";

  @override
  bool operator ==(dynamic other) => other is FormationPos && other.row == row && other.column == column;

  @override
  int get hashCode => toString().hashCode;
}
