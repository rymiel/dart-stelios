import "dart:math";

import "package:dart_console/src/key.dart";
import "package:piecemeal/piecemeal.dart";

import "const.dart";
import "ext.dart" show KeyName;
import "platform_specific/base.dart"
    if (dart.library.io) "platform_specific/native.dart"
    if (dart.library.html) "platform_specific/web.dart";

/// Console object from dart_console, to be wrapped by [Screen].
final Console console = Console();

/// Represents a single line drawn on a [Canvas]
class CanvasLine {
  /// Initializes a line for a [Canvas]
  CanvasLine(this._content, this.line, this.width, [this.prefix = "", this.suffix = ""]);

  bool _rendered = false;
  String _content;

  /// Appended to the content string when accessing it.
  String prefix;
  String suffix;
  String _finalSuffix = "";

  /// Width of the canvas it is made for.
  int width;

  String get combined => prefix + _content + suffix;

  /// Gets the content of the line, adding on the [prefix], padding the end with
  /// spaces to make sure the width of the line matches that of the [Canvas] and
  /// converts color escape sequences to ANSI codes.
  String get content => combined + (" " * (width - length)) + _finalSuffix;

  /// Set line content (excluding [prefix])
  set content(String text) {
    _content = text;
  }

  /// Length of the line contents, ignoring color escape seqences as they wont
  /// be rendered.
  int get length => Canvas.clength(combined);

  /// x-coordinate of the line on the relevant [Canvas].
  int line;
}

/// A rectangual area on the [Screen] that can be written to and rendered
/// independently of other [Canvas] objects.
class Canvas {
  /// Creates an empty canvas.
  Canvas();

  /// Creates a canvas for the [screen], for where the pages go.
  Canvas.forScreen(Screen screen) {
    width = screen.width;
    height = screen.height - 1;
    x = 0;
    y = Screen._header + 1;
    clear();
  }

  /// Width of the area
  int width;

  /// Height of the area
  int height;

  /// Column of the [Screen] the first column of this is on.
  int x;

  /// Row of the [Screen] the top row of this is on.
  int y;

  /// Lines written on this canvas
  List<CanvasLine> content = [];

  int _atY = 0;

  /// Obsolete.
  int get absX => x;

  /// Y-coordinate of the Canvas "cursor", where the next line would be written
  /// on the [Screen]
  int get absY => y + _atY;

  /// Empties the contents of this
  void clear() {
    content = List<CanvasLine>.generate(height, (i) => CanvasLine("", i, width));
  }

  /// Writes a string at the current Canvas cursor position.
  void writeLine(String text) {
    if (_atY < height) {
      content[_atY] = CanvasLine(text, _atY, width);
    }
    _atY++;
  }

  /// Draws all the unrendered lines of this to the [Screen].
  void render() =>
      (content.toList()
        ..removeWhere((item) => item._rendered)).forEach((text) {
        console
          ..cursorPosition = Vec(y + text.line, x)
          ..write(text.content)
          ..resetColor();
        text._rendered = true;
      });

  /// Marks all the lines of this to be not rendered.
  void derender() => content.forEach((line) => line._rendered = false);

  /// Gets the length of a string when ignoring color escape sequences.
  static int clength(String text) =>
      text.replaceAll(colorCodeRegex, "").replaceFirst("\n\$", "").length;

  static String ctruncate(String text, int chars) {
    var count = 0;
    final current = StringBuffer();
    var skip = 0;
    for (var i = 0; i < text.length; i++) {
      current.write(text[i]);
      if (skip > 0) {
        skip--;
        continue;
      }
      if (text.substring(i).startsWith(colorCodeRegex)) {
        skip = 2;
      } else {
        count++;
        if (count == chars) {
          return current.toString();
        }
      }
    }
    return current.toString();
  }

  /// Pads a string to a given with so that it appears in the center.
  /// (Ignores color escape sequences).
  static String padCenter(String text, int width) {
    final textLength = clength(text);
    final padding = ((width - textLength) / 2).round();
    return text.padLeft(textLength + padding).padRight(width);
  }
}

/// Handles the scrollable selection list on the right side of the game screen.
class Sidebar extends Canvas {
  /// Creates a sidebar for the [Screen] of the game
  Sidebar.forScreen(Screen screen) : super.forScreen(screen) {
    width = screen.sidebarWidth;
    height = screen.height;
    x = screen.width;
    y = Screen._header;
    options = [];
  }

  /// Color of the background of the list.
  static String accentColor = Screen.accentColor;

  /// Color of the background of the select item in the list.
  String focusAccentColor = "%BB";
  List<String> _options = [];
  List<CanvasLine> _content = [];
  int _focus = 0;
  int _prevOverflow;

  /// Sets the possible options to appear on the sidebar.
  set options(List<String> strings) {
    _options = strings;
    focus = 0;
    _prevOverflow = null;
  }

  /// Current options.
  List<String> get options => _options;

  /// Changes the index of the selected option.
  set focus(int newFocus) {
    var index = newFocus;
    if (index >= _options.length) {
      index = 0;
    } else if (index < 0) {
      index = _options.length - 1;
    }
    setView(index);
    _focus = index;
  }

  /// Index of the currently selected option.
  int get focus => _focus;

  /// Converts the string options into [CanvasLine] objects to be rendered.
  @override
  List<CanvasLine> get content {
    _content.forEach((line) {
      if (line.prefix != accentColor) {
        line
          .._rendered = false
          ..prefix = accentColor;
      }
    });
    if (options.isNotEmpty) {
      _content[focus - (_prevOverflow ?? 0)]
        ..prefix = focusAccentColor
        .._rendered = false;
    }
    return _content;
  }

  /// Directly changes sidebar contents. Probably not something that should be
  /// done directly.
  @override
  set content(List<CanvasLine> lines) {
    _content = lines;
  }

  /// Moves the rendered portion of the options so that [viewFocus] is certainly
  /// in view. Intelligently derenderes only when necessary.
  void setView([int viewFocus = 0]) {
    final overflow = (viewFocus - (height ~/ 2)).clamp(0, max(0, _options.length - height));
    if (overflow != _prevOverflow) {
      final optionsView = List<String>.from(_options)
        ..removeRange(0, overflow)
        ..length = height;
      _content =
      List<CanvasLine>.generate(height, (i) =>
      CanvasLine(optionsView[i] ?? "", i, width)
        .._finalSuffix = "%XE");
    }
    _prevOverflow = overflow;
  }

  @override
  void clear() {
    options = [];
    content = List<CanvasLine>.generate(height, (i) => CanvasLine("${' ' * width}%XE", i, width));
    _prevOverflow = null;
  }

  /// Deprecated. Use [options] setter instead.
  @override
  void writeLine(String text) {
    if (_atY < height) {
      content[_atY] = CanvasLine(text, _atY, width);
    }
    _atY++;
  }
}

class DialogOption {
  DialogOption(this.key, this.function);

  final void Function(Screen) function;
  final Key key;
}

class DialogBox {
  DialogBox(this.screen, {this.title, this.accentColor, this.content, this.options}) {
    title ??= "Error!";
    accentColor ??= "R";
  }

  final Screen screen;
  String title;
  String accentColor;
  final String content;
  final Map<String, DialogOption> options;

  int get width => screen.width - 6; // (account for some padding)

  void render() {
    // Mark all of the currently displayed content as not rendered, meaning it will all be rendered again once the
    // dialog box disappears. Dialog boxes are probably rare enough for this re-render overhead to
    // not be an actual issue.
    screen.pages[screen.currentPage].derender();
    final splitText = content.split("\n");
    var newText = <String>[];
    splitText.forEach((t) => newText.addAll(Screen._wrap(t, width - 2)));
    newText = newText.map((t) => "%fX%BW $t ").toList()
      ..insert(0, "%BW")
      ..add("%BW")
      ..insert(0, "%FW%B$accentColor $title ")
      ..insert(0, "")
      ..addAll(options.entries.map((e) =>
          "%FY%B$accentColor %SB[ ${e.value.key.name} ]%SE%FW ${e.key} "))
      ..add("");
    final maxLine = newText.map(Canvas.clength).reduce(max);
    if (maxLine > width) {
      String longestArgument;
      final argumentLengths = newText.map((i) => [i, Canvas.clength(i)]);
      for (final i in argumentLengths) {
        if (i.last == maxLine) {
          longestArgument = i.first;
        }
      }
      throw ArgumentError.value(longestArgument,
          "An option is wider ($maxLine) than the possible width ($width)");
    }
    final xOffset = (screen.width - maxLine - 2) ~/ 2;
    var yOffset = (screen.height - newText.length) ~/ 2 + Screen._header;
    newText
        .map((t) =>
            " ${t.padRight(maxLine + (t.length - Canvas.clength(t)))} %XE")
        .forEach((t) {
      console.cursorPosition = Vec(yOffset++, xOffset);
      console.write(t);
    });
    console.write("\u{7}");

    Future<bool> catchInputs() async {
      var displaying = true;
      Key key;
      while (displaying) {
        key = await screen.getInput(enableNavigation: false);
        options.values.forEach((i) {
          if (i.key.toString() == key.toString()) {
            if (i.function != null) {
              i.function(screen);
            }
            displaying = false;
          }
        });
      }
      return true;
    }

    catchInputs().then((success) {
      if (success) {
        // If we've made it this far the dialog box should be closed now, so render the real page over the dialog bog
        screen.pages[screen.currentPage].render();
      } else {
        throw ArgumentError(
            "Catching input from dialog box didn't return a success");
      }
    });
  }
}

/// Wraps [console]. There should only be a single screen instance for the whole
/// game.
class Screen {
  /// Creates a screen given the secondary color and the title of the game
  /// window.
  Screen(String title) {
    console
      ..init()
      ..clearScreen()
      ..write("$accentColor%FW%SB${Canvas.padCenter(title, windowWidth)}%XE\n");
    clearExtraLine();
    addPage();
    sidebar = Sidebar.forScreen(this)
      ..render();
  }

  /// How wide the sidebar on the right should be.
  final int sidebarWidth = ((console.windowWidth - 80) / 9 + 7).clamp(7, 24).round();
  static const _header = 1;
  static const _footer = 5;

  /// maximum amount of pages that can be created before the last ones are
  /// deleted.
  static int maxPages = 10;

  /// List of page canvases.
  List<Canvas> pages = [];

  /// [Sidebar] object that appears on the right.
  Sidebar sidebar;

  /// Index of the page this is current focused on.
  int currentPage = 0;

  /// Width of the area dedicated for the pages
  int get width => console.windowWidth - sidebarWidth;

  /// Height of the area dedicated for the pages
  int get height => console.windowHeight - _header - _footer;

  int get windowWidth => console.windowWidth;

  int get windowHeight => console.windowHeight;

  /// Gets last page, usually the one incomplete and being written to.
  Canvas get lastPage => pages.last;

  bool get onLastPage => currentPage == pages.length - 1;

  /// Appends a page to be the last page, removing the first one if over the
  /// limit of [maxPages].
  set lastPage(Canvas page) {
    pages.add(page);
    if (pages.length > maxPages) {
      pages.removeAt(0);
    }
  }

  /// Color escape sequence used for the header, sidebar and footer.
  static String accentColor = "%bB";
  String _extraLine;

  String get extraLine => _extraLine;

  set extraLine(String text) {
    _extraLine = text;
    if (onLastPage) {
      text != null ? renderExtraLine(text) : clearExtraLine();
    }
  }

  /// Generates a new [Canvas] that can be a page for this.
  Canvas newPage() => Canvas.forScreen(this);

  /// Generates a new [Canvas], adds it to the list of pages, and clears the
  /// page area of this.
  void addPage() => lastPage = newPage()..render();

  void clearExtraLine() => console
    ..cursorPosition = Vec(console.windowHeight - _footer, 0)
    ..write("$accentColor${" " * console.windowWidth}%XE");

  /// Write [text] to the last page of the screen, appending [newLines] of empty
  /// lines after it.
  void write(String text, {int newLines = 0, bool nonbreaking = false}) {
    final splitText = text.split("\n");
    final newText = [];
    final writingAt = pages[currentPage];
    // bool continued;
    splitText.forEach((t) => newText.addAll(_wrap(t, width)));
    if (nonbreaking && (lastPage._atY + newText.length >= height - 1)) {
      appendPage();
      // continued = true;
    }
    for (final t in newText) {
      if (lastPage._atY >= height - 1) {
        appendPage();
        // continued = true;
      }
      lastPage.writeLine(t);
    }
    writingAt.render();
    lastPage._atY += newLines;
  }

  void renderExtraLine(String text) {
    clearExtraLine();
    final colorText = "$accentColor %SB%FW* $text%XE";
    console
      ..cursorPosition = Vec(console.windowHeight - _footer, 0)
      ..write(colorText);
  }

  void renderDialogBox({String title,
    String accentColor,
    String content,
    List<String> optionNames,
    List<Key> optionKeys,
    List<void Function(Screen)> optionFunctions}) {
    // ignore: unrelated_type_equality_checks
    assert((optionNames.length == optionKeys.length) ==
        optionFunctions.length, "option params must be of equal length");
    assert(colorCodeLookup.containsKey(accentColor), "accentColor must be a valid background color");
    final options = <String, DialogOption>{};
    for (var i = 0; i < optionNames.length; ++i) {
      options[optionNames[i]] = DialogOption(optionKeys[i], optionFunctions[i]);
    }
    DialogBox(
        this,
        title: title,
        accentColor: accentColor,
        content: content,
        options: options
    ).render();
  }

  /// For development. Makes it so nothing ever happens again except for exiting
  /// with ctrlQ or ctrlZ.
  void lock() {
    Future<void> inputForever() async {
      for (;;) {
        await getInput();
      }
    }

    extraLine = "End of demo";
    inputForever();
  }

  /// Get a single key of input, also intercepting exiting characters to quit
  /// the game and left and right arrow characters to change the page, if enabled.
  Future<Key> getInput({bool enableNavigation = true}) async {
    var finished = false;
    Key key;
    while (!finished) {
      key = await console.readKey();

      if (key.controlChar == ControlCharacter.ctrlC ||
          key.controlChar == ControlCharacter.ctrlQ) {
        quit(1);
      } else
      if (key.controlChar == ControlCharacter.arrowRight && enableNavigation) {
        moveFocus(1);
      } else
      if (key.controlChar == ControlCharacter.arrowLeft && enableNavigation) {
        moveFocus(-1);
      } else {
        if (onLastPage || !enableNavigation) {
          finished = true;
        }
      }
    }
    return key;
  }

  /// Get an option from the sidebar.
  Future<String> getSidebar() async {
    Key key;
    for (;;) {
      key = await getInput();
      if (key.controlChar != ControlCharacter.enter) {
        sidebar.render();
        if (key.controlChar == ControlCharacter.arrowDown) {
          sidebar.focus += 1;
        } else if (key.controlChar == ControlCharacter.arrowUp) {
          sidebar.focus -= 1;
        }
      } else {
        return sidebar.options[sidebar.focus];
      }
    }
  }

  Future<void> enterToContinue() async {
    extraLine = "Press enter to continue";
    Key key;
    for (;;) {
      key = await getInput();
      if (key.controlChar == ControlCharacter.enter) {
        extraLine = null;
        return;
      }
    }
  }

  void setFocus(int newFocus, {bool renderPage = true}) {
    if (renderPage) {
      pages[currentPage].derender();
      pages[newFocus].render();
    }
    currentPage = newFocus;
    if (newFocus < pages.length - 1) {
      renderExtraLine("Continued");
    } else if (extraLine == null) {
      clearExtraLine();
    } else {
      renderExtraLine(extraLine);
    }
  }

  void moveFocus(int difference, {bool renderPage = true}) {
    setFocus((currentPage + difference).clamp(0, pages.length - 1), renderPage: renderPage);
  }

  void appendPage({bool renderPage = false}) {
    lastPage = newPage();
    moveFocus(0, renderPage: renderPage);
  }

  /// Clean everything up for quitting and quit the program.
  void quit(int code) {
    console
      ..clearScreen()..cleanUp()..exit(code);
  }

  void displayAndExit(String message) {
    console
      ..clearScreen()
      ..cleanUp()
      ..simpleMode()
      ..write("$message\nAny key to exit.".split("\n").map((i) =>
          _wrap(i, console.windowWidth).join("\n")).join("\n"));
    getInput(enableNavigation: false).then((_) => quit(1));
  }

  static List<String> _wrap(String text, int chars) {
    var count = 0;
    final current = StringBuffer();
    var skip = 0;
    for (var i = 0; i < text.length; i++) {
      current.write(text[i]);
      if (skip > 0) {
        skip--;
        continue;
      }
      if (text.substring(i).startsWith(colorCodeRegex)) {
        skip = 2;
      } else {
        count++;
        if (count == chars) {
          count = 0;
          if (i != text.length - 1) {
            current.writeln();
          }
        }
      }
    }

    return current.toString().split("\n");
  }
}
