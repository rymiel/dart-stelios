import "dart:math";

import "ext.dart" show stitchStrings;
import "party.dart";
import "path.dart";
import "screen.dart";

String box(int height, String line) => List.filled(height, line).join("\n");

class Battle {
  Battle(this.room, this.encounter) {
    /* screen.write("$playerParty are fighting ${encounter.all.join(', ')}.");
    encounter.all.forEach((i) => {
          if (i != null)
            {screen.write(i.stat.statBase.vals.toString(), newLines: 1)}
        }); */
    final playerFormationDescription = playerParty.formation
        .describe(screen.width ~/ 2 - 4, 5, divider: "%fG:%XE");
    final enemyFormationDescription = encounter
        .describe(screen.width ~/ 2 - 4, 5, flipped: true, divider: "%fR:%XE");
    final centerPad = box(
        max(playerParty.formation.width, encounter.width),
        " " *
            ((screen.width -
                    Canvas.clength(
                            playerFormationDescription.split("\n").first) *
                        2 -
                    6) ~/
                2));
    final playerBorder = box(playerParty.formation.width, "%FG|%XE");
    final enemyBorder = box(encounter.width, "%FR|%XE");
    screen.write(
        stitchStrings([
          playerBorder,
          playerFormationDescription,
          playerBorder,
          centerPad,
          "\nVS",
          centerPad,
          enemyBorder,
          enemyFormationDescription,
          enemyBorder,
        ], lengthFunc: Canvas.clength),
        nonbreaking: true);
  }

  Room room;
  Formation encounter;

  Party get playerParty => room.playerParty;

  Screen get screen => room.playerParty.screen;
}
