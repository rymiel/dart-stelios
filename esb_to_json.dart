import "dart:convert";
import "dart:io";

import "package:stelios/esb.dart";

void main(List<String> params) {
  final esb = decodeFromFile(params[0]);
  final withHeader = {"header": esb.header, "content": esb.content};
  final json = jsonEncode(withHeader);
  File(params[1]).writeAsStringSync(json);
}
